FROM maven:3.6.3-jdk-11
WORKDIR /app
COPY my-app/target/*.jar /app/docker-task.jar
CMD ["java","-jar","/app/docker-task.jar"]
